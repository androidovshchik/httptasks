package ru.mirsapera.extension

import android.content.Context
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip
import ru.mirsapera.R

fun Context.newTooltip(text: String): SimpleTooltip.Builder =
    SimpleTooltip.Builder(applicationContext)
        .contentView(R.layout.tooltip, R.id.tv_tooltip)
        .text(text)
        .showArrow(false)
        .dismissOnOutsideTouch(false)