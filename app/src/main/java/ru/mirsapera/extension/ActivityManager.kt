@file:Suppress("unused", "DEPRECATION")

package ru.mirsapera.extension

import android.annotation.SuppressLint
import android.app.ActivityManager
import android.app.Service

inline fun <reified T : Service> ActivityManager.isRunning(): Boolean {
    for (service in getRunningServices(Integer.MAX_VALUE)) {
        if (T::class.java.name == service.service.className) {
            return true
        }
    }
    return false
}

@SuppressLint("NewApi")
fun ActivityManager.getTopActivity(packageName: String): String? {
    for (task in getRunningTasks(Integer.MAX_VALUE)) {
        task.topActivity?.let {
            if (it.packageName == packageName) {
                return it.className
            }
        }
    }
    return null
}