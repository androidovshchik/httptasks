package ru.mirsapera.extension

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import ru.mirsapera.R

val FragmentManager.topFragment: Fragment?
    get() = findFragmentByTag((backStackEntryCount - 1).toString())

fun FragmentManager.putFragment(fragment: Fragment) {
    beginTransaction()
        .replace(R.id.fl_container, fragment, backStackEntryCount.toString())
        .addToBackStack(fragment.javaClass.name)
        .commitAllowingStateLoss()
    executePendingTransactions()
}

fun FragmentManager.popFragment(name: String?, immediate: Boolean): Boolean {
    return if (backStackEntryCount > 0) {
        if (name != null) {
            if (immediate) {
                popBackStackImmediate(name, 0)
            } else {
                popBackStack(name, 0)
            }
        } else {
            if (immediate) {
                popBackStackImmediate()
            } else {
                popBackStack()
            }
        }
        true
    } else {
        false
    }
}