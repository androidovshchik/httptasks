@file:Suppress("unused", "DEPRECATION")

package ru.mirsapera.extension

import android.net.ConnectivityManager

val ConnectivityManager.isConnected: Boolean
    get() = activeNetworkInfo?.isConnected == true