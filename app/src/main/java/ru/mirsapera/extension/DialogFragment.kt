package ru.mirsapera.extension

import androidx.fragment.app.DialogFragment

fun DialogFragment.close() {
    try {
        dismissAllowingStateLoss()
    } catch (e: Throwable) {
    }
}