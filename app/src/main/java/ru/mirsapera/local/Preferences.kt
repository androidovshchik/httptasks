package ru.mirsapera.local

import android.content.Context
import com.chibatching.kotpref.KotprefModel
import ru.mirsapera.extension.md5

class Preferences(context: Context) : KotprefModel(context) {

    override val kotprefName: String = "${context.packageName}_preferences"

    var uid by longPref(0L, "uid")

    var email by nullableStringPref(null, "email")

    val hash: String
        get() = "$uid$email".md5()
}