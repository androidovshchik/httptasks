package ru.mirsapera.screen

import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.contexted
import org.kodein.di.generic.provider
import ru.mirsapera.screen.main.MainActivity
import ru.mirsapera.screen.main.MainPresenter
import ru.mirsapera.screen.menu.MenuFragment
import ru.mirsapera.screen.menu.MenuPresenter
import ru.mirsapera.screen.note.NoteDialog
import ru.mirsapera.screen.note.NotePresenter
import ru.mirsapera.screen.settings.SettingsDialog
import ru.mirsapera.screen.settings.SettingsPresenter
import ru.mirsapera.screen.task.TaskFragment
import ru.mirsapera.screen.task.TaskPresenter
import ru.mirsapera.screen.tasks.TasksFragment
import ru.mirsapera.screen.tasks.TasksPresenter

val screenModule = Kodein.Module("screen") {

    bind<MainPresenter>() with contexted<MainActivity>().provider {
        MainPresenter(context).apply {
            attachView(context)
        }
    }

    bind<MenuPresenter>() with contexted<MenuFragment>().provider {
        MenuPresenter(context.requireContext()).apply {
            attachView(context)
        }
    }

    bind<TasksPresenter>() with contexted<TasksFragment>().provider {
        TasksPresenter(context.requireContext()).apply {
            attachView(context)
        }
    }

    bind<TaskPresenter>() with contexted<TaskFragment>().provider {
        TaskPresenter(context.requireContext()).apply {
            attachView(context)
        }
    }

    bind<SettingsPresenter>() with contexted<SettingsDialog>().provider {
        SettingsPresenter(context.requireContext()).apply {
            attachView(context)
        }
    }

    bind<NotePresenter>() with contexted<NoteDialog>().provider {
        NotePresenter(context.requireContext()).apply {
            attachView(context)
        }
    }
}