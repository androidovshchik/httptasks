package ru.mirsapera.screen.settings

import android.content.Context
import com.chibatching.kotpref.bulk
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import org.kodein.di.generic.instance
import ru.mirsapera.extension.cancelAll
import ru.mirsapera.local.Preferences
import ru.mirsapera.remote.Server
import ru.mirsapera.screen.base.BasePresenter

@Suppress("BlockingMethodInNonBlockingContext")
class SettingsPresenter(context: Context) : BasePresenter<SettingsContract.View>(context),
    SettingsContract.Presenter {

    private val preferences: Preferences by instance()

    private val server: Server by instance()

    private val client: OkHttpClient by instance()

    override fun registerEmail(email: String) {
        job.cancelChildren()
        client.cancelAll("EMAIL")
        launch {
            val rEmail = server.register(email)
            if (rEmail.isSuccessful) {
                preferences.bulk {
                    uid = rEmail.uid?.toLong() ?: 0L
                    this.email = email
                }
            }
            reference.get()?.onEmail(rEmail)
        }
    }

    override fun changeEmail(email: String) {
        job.cancelChildren()
        client.cancelAll("EMAIL")
        launch {
            val rEmail = server.changeEmail(
                preferences.uid,
                preferences.email.toString(),
                email,
                preferences.hash
            )
            if (rEmail.isSuccessful) {
                preferences.email = email
            }
            reference.get()?.onEmail(rEmail)
        }
    }

    override fun detachView() {
        reference.clear()
    }
}