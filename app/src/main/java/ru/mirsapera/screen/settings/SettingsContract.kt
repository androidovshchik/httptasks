package ru.mirsapera.screen.settings

import ru.mirsapera.remote.dto.RStatus
import ru.mirsapera.screen.base.listeners.IPresenter
import ru.mirsapera.screen.base.listeners.IView

interface SettingsContract {

    interface View : IView {

        fun onEmail(rStatus: RStatus?)
    }

    interface Presenter : IPresenter<View> {

        fun registerEmail(email: String)

        fun changeEmail(email: String)
    }
}