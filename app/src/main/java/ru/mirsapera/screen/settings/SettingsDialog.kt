package ru.mirsapera.screen.settings

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.dialog_menu.*
import org.jetbrains.anko.toast
import org.kodein.di.generic.instance
import ru.mirsapera.R
import ru.mirsapera.extension.close
import ru.mirsapera.extension.setTextSelection
import ru.mirsapera.local.Preferences
import ru.mirsapera.remote.dto.RStatus
import ru.mirsapera.screen.base.BaseDialogFragment

class SettingsDialog : BaseDialogFragment<SettingsContract.Presenter>(R.style.SettingsDialog),
    SettingsContract.View {

    override val presenter: SettingsPresenter by instance()

    private val preferences: Preferences by instance()

    override fun onCreateView(inflater: LayoutInflater, root: ViewGroup?, bundle: Bundle?): View? {
        super.onCreateView(inflater, root, bundle)
        return inflater.inflate(R.layout.dialog_menu, root, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        ib_cancel.setOnClickListener {
            close()
        }
        ib_done.setOnClickListener {
            val email = et_email.text.toString()
            if (preferences.uid <= 0) {
                presenter.registerEmail(email)
            } else {
                presenter.changeEmail(email)
            }
        }
        onEmail(null)
    }

    @SuppressLint("SetTextI18n")
    override fun onEmail(rStatus: RStatus?) {
        tv_uid.text = "UID: ${preferences.uid}"
        et_email.setTextSelection(preferences.email)
        if (rStatus != null) {
            rStatus.message?.let {
                context?.toast(it)
            }
            if (rStatus.isSuccessful) {
                close()
            }
        }
    }
}