package ru.mirsapera.screen.tasks

import ru.mirsapera.screen.base.listeners.IPresenter
import ru.mirsapera.screen.base.listeners.IView

interface TasksContract {

    interface View : IView

    interface Presenter : IPresenter<View>
}