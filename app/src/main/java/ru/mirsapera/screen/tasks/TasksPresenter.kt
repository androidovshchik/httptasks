package ru.mirsapera.screen.tasks

import android.content.Context
import ru.mirsapera.screen.base.BasePresenter

class TasksPresenter(context: Context) : BasePresenter<TasksContract.View>(context),
    TasksContract.Presenter