package ru.mirsapera.screen.tasks

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip
import kotlinx.android.synthetic.main.fragment_tasks.*
import org.kodein.di.generic.instance
import ru.mirsapera.R
import ru.mirsapera.extension.newTooltip
import ru.mirsapera.remote.dto.RActivity
import ru.mirsapera.screen.base.BaseFragment
import ru.mirsapera.screen.task.TaskFragment
import kotlin.math.max
import kotlin.math.min

class TasksFragment : BaseFragment<TasksContract.Presenter>(), TasksContract.View {

    override val presenter: TasksPresenter by instance()

    private lateinit var rActivity: RActivity

    private lateinit var tooltip: SimpleTooltip

    private var charIndex = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        rActivity = args.getSerializable("activity") as RActivity
    }

    override fun onCreateView(inflater: LayoutInflater, root: ViewGroup?, bundle: Bundle?): View {
        return inflater.inflate(R.layout.fragment_tasks, root, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        tooltip = view.context.newTooltip("Вот список задач")
            .anchorView(v_button)
            .gravity(Gravity.END)
            .margin(0f)
            .build()
        mb_prev.setOnClickListener {
            tooltip.dismiss()
            charIndex = max(0, charIndex - 1)
            updateTask()
        }
        mb_next.setOnClickListener {
            tooltip.dismiss()
            charIndex = min(rActivity.character.size - 1, charIndex + 1)
            updateTask()
        }
        updateTask()
        tooltip.show()
    }

    private fun updateTask() {
        mb_prev.visibility = if (charIndex > 0) View.VISIBLE else View.INVISIBLE
        mb_next.visibility =
            if (charIndex < rActivity.character.size - 1) View.VISIBLE else View.INVISIBLE
        while (popFragment());
        rActivity.character.getOrNull(charIndex)?.also {
            putFragment(TaskFragment.newInstance(it))
        }
    }

    override fun onDestroyView() {
        tooltip.dismiss()
        super.onDestroyView()
    }

    companion object {

        fun newInstance(rActivity: RActivity): TasksFragment {
            return TasksFragment().apply {
                arguments = Bundle().apply {
                    putSerializable("activity", rActivity)
                }
            }
        }
    }
}