package ru.mirsapera.screen.task

import android.content.ClipData
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import coil.api.load
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip
import kotlinx.android.synthetic.main.fragment_task.*
import org.jetbrains.anko.clipboardManager
import org.jetbrains.anko.toast
import org.kodein.di.generic.instance
import ru.mirsapera.R
import ru.mirsapera.extension.newTooltip
import ru.mirsapera.local.Preferences
import ru.mirsapera.remote.dto.RCharacter
import ru.mirsapera.screen.base.BaseFragment
import ru.mirsapera.service.SendWorker

class TaskFragment : BaseFragment<TaskContract.Presenter>(), TaskContract.View {

    override val presenter: TaskPresenter by instance()

    private val preferences: Preferences by instance()

    private var tooltip: SimpleTooltip? = null

    lateinit var char: RCharacter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        char = args.getSerializable("char") as RCharacter
    }

    override fun onCreateView(inflater: LayoutInflater, root: ViewGroup?, bundle: Bundle?): View {
        return inflater.inflate(R.layout.fragment_task, root, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        tv_name.text = char.name.toString()
        tv_desc.text = char.desc.toString()
        char.phone?.let { phone ->
            ib_phone.apply {
                visibility = View.VISIBLE
                setOnClickListener {
                    context?.clipboardManager?.setPrimaryClip(ClipData.newPlainText(null, phone))
                    tooltip = it.context.newTooltip(phone)
                        .anchorView(it)
                        .gravity(Gravity.BOTTOM)
                        .dismissOnOutsideTouch(true)
                        .build()
                    tooltip?.show()
                    context?.toast("Скопировано в буфер обмена")
                }
            }
        }
        char.link?.let { link ->
            ib_link.apply {
                visibility = View.VISIBLE
                setOnClickListener {
                    context?.clipboardManager?.setPrimaryClip(ClipData.newPlainText(null, link))
                    tooltip = it.context.newTooltip(link)
                        .anchorView(it)
                        .gravity(Gravity.BOTTOM)
                        .dismissOnOutsideTouch(true)
                        .build()
                    tooltip?.show()
                    context?.toast("Скопировано в буфер обмена")
                }
            }
        }
        char.img?.let {
            iv_image.apply {
                visibility = View.VISIBLE
                load(it)
            }
        }
        char.count.firstOrNull { it.iteration != null }?.let {
            ll_count.visibility = View.VISIBLE
            tv_count1.apply {
                visibility = View.VISIBLE
                text = it.iteration.toString()
                setOnClickListener {
                    tooltip = it.context.newTooltip("Количество повторов")
                        .anchorView(it)
                        .gravity(Gravity.TOP)
                        .dismissOnOutsideTouch(true)
                        .build()
                    tooltip?.show()
                }
            }
        }
        char.count.firstOrNull { it.rejected != null }?.let {
            ll_count.visibility = View.VISIBLE
            tv_count2.apply {
                visibility = View.VISIBLE
                text = it.rejected.toString()
                setOnClickListener {
                    tooltip = it.context.newTooltip("Уникальность")
                        .anchorView(it)
                        .gravity(Gravity.TOP)
                        .dismissOnOutsideTouch(true)
                        .build()
                    tooltip?.show()
                }
            }
        }
        mb_cancel.setOnClickListener {
            SendWorker.launch(
                it.context, mapOf(
                    "id" to char.id,
                    "cancel" to true,
                    "note" to char.note,
                    "hash" to preferences.hash
                )
            )
        }
        mb_done.setOnClickListener {
            SendWorker.launch(
                it.context, mapOf(
                    "id" to char.id,
                    "cancel" to false,
                    "note" to char.note,
                    "hash" to preferences.hash
                )
            )
        }
    }

    override fun onDestroyView() {
        tooltip?.dismiss()
        super.onDestroyView()
    }

    companion object {

        fun newInstance(char: RCharacter): TaskFragment {
            return TaskFragment().apply {
                arguments = Bundle().apply {
                    putSerializable("char", char)
                }
            }
        }
    }
}