package ru.mirsapera.screen.task

import ru.mirsapera.screen.base.listeners.IPresenter
import ru.mirsapera.screen.base.listeners.IView

interface TaskContract {

    interface View : IView

    interface Presenter : IPresenter<View>
}
