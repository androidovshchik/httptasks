package ru.mirsapera.screen.task

import android.content.Context
import ru.mirsapera.screen.base.BasePresenter

class TaskPresenter(context: Context) : BasePresenter<TaskContract.View>(context),
    TaskContract.Presenter
