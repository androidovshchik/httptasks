package ru.mirsapera.screen.main

import ru.mirsapera.screen.base.listeners.IPresenter
import ru.mirsapera.screen.base.listeners.IView

interface MainContract {

    interface View : IView

    interface Presenter : IPresenter<View>
}
