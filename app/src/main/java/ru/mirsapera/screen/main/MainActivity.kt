package ru.mirsapera.screen.main

import android.os.Bundle
import coil.api.load
import coil.transform.CircleCropTransformation
import kotlinx.android.synthetic.main.activity_main.*
import org.kodein.di.generic.instance
import ru.mirsapera.R
import ru.mirsapera.extension.close
import ru.mirsapera.screen.base.BaseActivity
import ru.mirsapera.screen.menu.MenuFragment
import ru.mirsapera.screen.note.NoteDialog
import ru.mirsapera.screen.settings.SettingsDialog
import ru.mirsapera.screen.task.TaskFragment
import ru.mirsapera.screen.tasks.TasksFragment
import java.lang.ref.WeakReference

class MainActivity : BaseActivity<MainContract.Presenter>(), MainContract.View {

    override val presenter: MainPresenter by instance()

    private var settingsDialog: SettingsDialog? = null

    private var noteDialog: NoteDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iv_avatar.load("https://www.w3schools.com/w3images/avatar2.png") {
            transformations(CircleCropTransformation())
        }
        ib_menu.setOnClickListener {
            transact {
                settingsDialog = SettingsDialog().also {
                    remove(it)
                    it.show(this, it.javaClass.name)
                }
            }
        }
        fab_edit.setOnClickListener {
            transact {
                val char = ((topFragment as? TasksFragment)?.topFragment as? TaskFragment)?.char
                    ?: return@setOnClickListener
                noteDialog = NoteDialog().also {
                    remove(it)
                    it.reference = WeakReference(char)
                    it.show(this, it.javaClass.name)
                }
            }
        }
        bnv_main.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.action_home -> popFragment(MenuFragment::class.java.name, false)
            }
            true
        }
        putFragment(MenuFragment())
    }

    override fun onDestroy() {
        settingsDialog?.close()
        noteDialog?.close()
        super.onDestroy()
    }
}
