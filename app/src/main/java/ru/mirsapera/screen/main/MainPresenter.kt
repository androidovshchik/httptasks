package ru.mirsapera.screen.main

import android.content.Context
import ru.mirsapera.screen.base.BasePresenter

class MainPresenter(context: Context) : BasePresenter<MainContract.View>(context),
    MainContract.Presenter
