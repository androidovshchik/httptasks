package ru.mirsapera.screen.menu

import ru.mirsapera.remote.dto.RActivity
import ru.mirsapera.screen.base.listeners.IPresenter
import ru.mirsapera.screen.base.listeners.IView

interface MenuContract {

    interface View : IView {

        fun onActivity(rActivity: RActivity)
    }

    interface Presenter : IPresenter<View> {

        fun getActivity(period: String)
    }
}