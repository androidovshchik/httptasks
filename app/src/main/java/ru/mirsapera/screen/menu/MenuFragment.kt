package ru.mirsapera.screen.menu

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip
import kotlinx.android.synthetic.main.fragment_menu.*
import org.kodein.di.generic.instance
import ru.mirsapera.R
import ru.mirsapera.extension.makeCallback
import ru.mirsapera.extension.newTooltip
import ru.mirsapera.remote.dto.DayActivity
import ru.mirsapera.remote.dto.RActivity
import ru.mirsapera.remote.dto.WeekActivity
import ru.mirsapera.remote.dto.YearActivity
import ru.mirsapera.screen.base.BaseActivity
import ru.mirsapera.screen.base.BaseFragment
import ru.mirsapera.screen.tasks.TasksFragment

class MenuFragment : BaseFragment<MenuContract.Presenter>(), MenuContract.View {

    override val presenter: MenuPresenter by instance()

    private lateinit var tooltip: SimpleTooltip

    override fun onCreateView(inflater: LayoutInflater, root: ViewGroup?, bundle: Bundle?): View {
        return inflater.inflate(R.layout.fragment_menu, root, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        tooltip = view.context.newTooltip("Привет!\nВыбери задачи")
            .anchorView(v_menu)
            .gravity(Gravity.END)
            .margin(0f)
            .build()
        mb_tasks1.setOnClickListener {
            presenter.getActivity(DayActivity.PERIOD)
        }
        mb_tasks2.setOnClickListener {
            presenter.getActivity(WeekActivity.PERIOD)
        }
        mb_tasks3.setOnClickListener {
            presenter.getActivity(YearActivity.PERIOD)
        }
        tooltip.show()
    }

    override fun onActivity(rActivity: RActivity) {
        tooltip.dismiss()
        context?.makeCallback<BaseActivity<*>> {
            putFragment(TasksFragment.newInstance(rActivity))
        }
    }

    override fun onDestroyView() {
        tooltip.dismiss()
        super.onDestroyView()
    }
}