package ru.mirsapera.screen.menu

import android.content.Context
import kotlinx.coroutines.launch
import org.kodein.di.generic.instance
import ru.mirsapera.local.Preferences
import ru.mirsapera.remote.Server
import ru.mirsapera.remote.dto.WeekActivity
import ru.mirsapera.remote.dto.YearActivity
import ru.mirsapera.screen.base.BasePresenter

@Suppress("BlockingMethodInNonBlockingContext")
class MenuPresenter(context: Context) : BasePresenter<MenuContract.View>(context),
    MenuContract.Presenter {

    private val preferences: Preferences by instance()

    private val server: Server by instance()

    override fun getActivity(period: String) {
        launch {
            val url = server.getActivityLink(preferences.uid, period, preferences.hash).string()
            val rActivity = when (period) {
                WeekActivity.PERIOD -> server.getWeekActivityXml(url)
                YearActivity.PERIOD -> server.getYearActivityXml(url)
                else -> server.getDayActivityXml(url)
            }
            reference.get()?.onActivity(rActivity)
        }
    }
}