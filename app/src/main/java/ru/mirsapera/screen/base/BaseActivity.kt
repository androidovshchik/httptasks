package ru.mirsapera.screen.base

import android.content.IntentFilter
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import org.jetbrains.anko.longToast
import org.jetbrains.anko.toast
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein
import ru.mirsapera.extension.popFragment
import ru.mirsapera.extension.putFragment
import ru.mirsapera.extension.topFragment
import ru.mirsapera.screen.base.listeners.IMessage
import ru.mirsapera.screen.base.listeners.IPresenter
import ru.mirsapera.screen.base.listeners.IView
import ru.mirsapera.screen.screenModule
import ru.mirsapera.service.MessageReceiver

@Suppress("MemberVisibilityCanBePrivate", "LeakingThis")
abstract class BaseActivity<P : IPresenter<*>> : AppCompatActivity(), IView, KodeinAware, IMessage {

    private val parentKodein by closestKodein()

    override val kodein: Kodein by Kodein.lazy {

        extend(parentKodein)

        import(screenModule)
    }

    abstract val presenter: P

    private val messageReceiver = MessageReceiver(this)

    override fun onStart() {
        super.onStart()
        registerReceiver(messageReceiver, IntentFilter("$packageName.MESSAGE"))
    }

    override val topFragment: BaseFragment<*>?
        get() = supportFragmentManager.topFragment as? BaseFragment<*>

    override fun putFragment(fragment: BaseFragment<*>) {
        supportFragmentManager.putFragment(fragment)
    }

    override fun popFragment(name: String?, immediate: Boolean) =
        supportFragmentManager.popFragment(name, immediate)

    protected inline fun transact(action: FragmentTransaction.() -> Unit) {
        supportFragmentManager.beginTransaction().apply(action)
    }

    override fun showMessage(text: String) {
        toast(text)
    }

    override fun showError(e: Throwable) {
        longToast(e.localizedMessage ?: e.toString())
    }

    override fun onStop() {
        unregisterReceiver(messageReceiver)
        super.onStop()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                false
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1) {
            super.onBackPressed()
        } else {
            finish()
        }
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }
}