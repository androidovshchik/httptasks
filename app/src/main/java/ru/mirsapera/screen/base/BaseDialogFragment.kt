@file:Suppress("DEPRECATION")

package ru.mirsapera.screen.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentTransaction
import org.jetbrains.anko.longToast
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.closestKodein
import ru.mirsapera.extension.popFragment
import ru.mirsapera.extension.putFragment
import ru.mirsapera.extension.topFragment
import ru.mirsapera.screen.base.listeners.IPresenter
import ru.mirsapera.screen.base.listeners.IView

abstract class BaseDialogFragment<P : IPresenter<*>>(private val themeId: Int) : DialogFragment(),
    IView, KodeinAware {

    override val kodein by closestKodein()

    abstract val presenter: P

    override fun onCreate(savedInstance: Bundle?) {
        super.onCreate(savedInstance)
        setStyle(STYLE_NORMAL, themeId)
    }

    override fun onCreateView(inflater: LayoutInflater, root: ViewGroup?, bundle: Bundle?): View? {
        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_UNCHANGED)
        return null
    }

    override val topFragment: BaseFragment<*>?
        get() = childFragmentManager.topFragment as? BaseFragment<*>

    override fun putFragment(fragment: BaseFragment<*>) {
        childFragmentManager.putFragment(fragment)
    }

    override fun popFragment(name: String?, immediate: Boolean) =
        childFragmentManager.popFragment(name, immediate)

    protected inline fun transact(action: FragmentTransaction.() -> Unit) {
        childFragmentManager.beginTransaction().apply(action)
    }

    override fun showError(e: Throwable) {
        context?.longToast(e.localizedMessage ?: e.toString())
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }
}