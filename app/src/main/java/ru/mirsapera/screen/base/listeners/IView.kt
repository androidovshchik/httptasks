package ru.mirsapera.screen.base.listeners

import ru.mirsapera.screen.base.BaseFragment

interface IView {

    val topFragment: BaseFragment<*>?

    fun putFragment(fragment: BaseFragment<*>)

    fun popFragment(name: String? = null, immediate: Boolean = true): Boolean

    fun showError(e: Throwable)
}
