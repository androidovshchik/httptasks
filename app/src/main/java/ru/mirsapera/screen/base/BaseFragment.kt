package ru.mirsapera.screen.base

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import org.jetbrains.anko.longToast
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.closestKodein
import ru.mirsapera.extension.popFragment
import ru.mirsapera.extension.putFragment
import ru.mirsapera.extension.topFragment
import ru.mirsapera.screen.base.listeners.IPresenter
import ru.mirsapera.screen.base.listeners.IView

@Suppress("MemberVisibilityCanBePrivate")
abstract class BaseFragment<P : IPresenter<*>> : Fragment(), IView, KodeinAware {

    override val kodein by closestKodein()

    abstract val presenter: P

    protected val args: Bundle
        get() = arguments ?: Bundle()

    override val topFragment: BaseFragment<*>?
        get() = childFragmentManager.topFragment as? BaseFragment<*>

    override fun putFragment(fragment: BaseFragment<*>) {
        childFragmentManager.putFragment(fragment)
    }

    override fun popFragment(name: String?, immediate: Boolean) =
        childFragmentManager.popFragment(name, immediate)

    protected inline fun transact(action: FragmentTransaction.() -> Unit) {
        childFragmentManager.beginTransaction().apply(action)
    }

    override fun showError(e: Throwable) {
        context?.longToast(e.localizedMessage ?: e.toString())
    }

    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }
}