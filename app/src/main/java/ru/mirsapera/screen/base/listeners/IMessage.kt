package ru.mirsapera.screen.base.listeners

interface IMessage {

    fun showMessage(text: String)
}