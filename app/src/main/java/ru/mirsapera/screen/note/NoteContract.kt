package ru.mirsapera.screen.note

import ru.mirsapera.screen.base.listeners.IPresenter
import ru.mirsapera.screen.base.listeners.IView

interface NoteContract {

    interface View : IView

    interface Presenter : IPresenter<View>
}