package ru.mirsapera.screen.note

import android.content.Context
import ru.mirsapera.screen.base.BasePresenter

class NotePresenter(context: Context) : BasePresenter<NoteContract.View>(context),
    NoteContract.Presenter