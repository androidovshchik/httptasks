package ru.mirsapera.screen.note

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.dialog_note.*
import org.jetbrains.anko.sdk19.listeners.textChangedListener
import org.kodein.di.generic.instance
import ru.mirsapera.R
import ru.mirsapera.extension.close
import ru.mirsapera.extension.setTextSelection
import ru.mirsapera.remote.dto.RCharacter
import ru.mirsapera.screen.base.BaseDialogFragment
import java.lang.ref.WeakReference

class NoteDialog : BaseDialogFragment<NoteContract.Presenter>(R.style.NoteDialog),
    NoteContract.View {

    override val presenter: NotePresenter by instance()

    var reference: WeakReference<RCharacter>? = null

    override fun onCreateView(inflater: LayoutInflater, root: ViewGroup?, bundle: Bundle?): View? {
        super.onCreateView(inflater, root, bundle)
        return inflater.inflate(R.layout.dialog_note, root, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mb_back.setOnClickListener {
            close()
        }
        reference?.get()?.let { char ->
            et_note.setTextSelection(char.note)
            char.success.firstOrNull { it.repetition != null }?.let {
                ll_success.visibility = View.VISIBLE
                tv_success1.apply {
                    visibility = View.VISIBLE
                    text = it.repetition.toString()
                }
            }
            char.success.firstOrNull { it.unique != null }?.let {
                ll_success.visibility = View.VISIBLE
                tv_success2.apply {
                    visibility = View.VISIBLE
                    text = it.unique.toString()
                }
            }
        }
        et_note.textChangedListener {
            afterTextChanged {
                reference?.get()?.note = it.toString()
            }
        }
    }
}