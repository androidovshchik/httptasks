package ru.mirsapera.remote

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.tickaroo.tikxml.TikXml
import com.tickaroo.tikxml.retrofit.TikXmlConverterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.mirsapera.BuildConfig
import timber.log.Timber

val remoteModule = Kodein.Module("remote") {

    bind<Gson>() with provider {
        GsonBuilder()
            .setLenient()
            .setExclusionStrategies(SerializedNameStrategy())
            .create()
    }

    bind<TikXml>() with provider {
        TikXml.Builder()
            .exceptionOnUnreadXml(false)
            .build()
    }

    bind<OkHttpClient>() with singleton {
        OkHttpClient.Builder().apply {
            addInterceptor(TagInterceptor())
            if (BuildConfig.DEBUG) {
                addInterceptor(HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message ->
                    Timber.tag("NETWORK")
                        .d(message)
                }).apply {
                    level = HttpLoggingInterceptor.Level.BODY
                })
                addNetworkInterceptor(
                    Class.forName("com.facebook.stetho.okhttp3.StethoInterceptor")
                        .newInstance() as Interceptor
                )
            }
        }.build()
    }

    bind<Server>() with singleton {
        Retrofit.Builder()
            .client(instance())
            .baseUrl("http://mirsapera.ru/")
            .addConverterFactory(
                MultipleConverterFactory.Builder()
                    .setXmlConverterFactory(TikXmlConverterFactory.create(instance()))
                    .setJsonConverterFactory(GsonConverterFactory.create(instance()))
                    .build()
            )
            .build()
            .create(Server::class.java)
    }
}