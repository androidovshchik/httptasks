@file:Suppress("unused")

package ru.mirsapera.remote

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import retrofit2.http.Url
import ru.mirsapera.remote.dto.DayActivity
import ru.mirsapera.remote.dto.RStatus
import ru.mirsapera.remote.dto.WeekActivity
import ru.mirsapera.remote.dto.YearActivity
import ru.mirsapera.service.SendWorker

interface Server {

    @JsonResponse
    @Tag("EMAIL")
    @GET("connector.php?q=reg")
    suspend fun register(
        @Query("email") email: String
    ): RStatus

    @JsonResponse
    @Tag("EMAIL")
    @GET("connector.php?q=chemail")
    suspend fun changeEmail(
        @Query("uid") uid: Long,
        @Query("old_email") oldEmail: String,
        @Query("email") newEmail: String,
        @Query("hash") hash: String
    ): RStatus

    @GET("connector.php?q=receive")
    suspend fun getActivityLink(
        @Query("uid") uid: Long,
        @Query("activity") activity: String,
        @Query("hash") hash: String
    ): ResponseBody

    @XmlResponse
    @GET
    suspend fun getDayActivityXml(
        @Url url: String
    ): DayActivity

    @XmlResponse
    @GET
    suspend fun getWeekActivityXml(
        @Url url: String
    ): WeekActivity

    @XmlResponse
    @GET
    suspend fun getYearActivityXml(
        @Url url: String
    ): YearActivity

    @JsonResponse
    @Tag(SendWorker.NAME)
    @POST("connector.php?q=send")
    fun sendTask(
        @Query("id") id: Int,
        @Query("cansel") cancel: Boolean,
        @Query("note") note: String,
        @Query("hash") hash: String
    ): Call<RStatus>
}