package ru.mirsapera.remote.dto

import com.tickaroo.tikxml.annotation.*
import java.io.Serializable

abstract class RActivity : Serializable {

    abstract var title: String?

    abstract var character: MutableList<RCharacter>
}

@Xml(name = "activity")
class DayActivity : RActivity() {

    @Path(PERIOD)
    @PropertyElement(name = "title")
    override var title: String? = null

    @Path("$PERIOD/characters")
    @Element
    override var character = mutableListOf<RCharacter>()

    companion object {

        const val PERIOD = "everyday"
    }
}

@Xml(name = "activity")
class WeekActivity : RActivity() {

    @Path(PERIOD)
    @PropertyElement(name = "title")
    override var title: String? = null

    @Path("$PERIOD/characters")
    @Element
    override var character = mutableListOf<RCharacter>()

    companion object {

        const val PERIOD = "everyweek"
    }
}

@Xml(name = "activity")
class YearActivity : RActivity() {

    @Path(PERIOD)
    @PropertyElement(name = "title")
    override var title: String? = null

    @Path("$PERIOD/characters")
    @Element
    override var character = mutableListOf<RCharacter>()

    companion object {

        const val PERIOD = "everyyear"
    }
}

@Xml(name = "character")
class RCharacter : Serializable {

    @PropertyElement(name = "id")
    var id = 0

    @PropertyElement(name = "name")
    var name: String? = null

    @PropertyElement(name = "desc")
    var desc: String? = null

    @PropertyElement(name = "phone")
    var phone: String? = null

    @PropertyElement(name = "link")
    var link: String? = null

    @PropertyElement(name = "img")
    var img: String? = null

    @PropertyElement(name = "note")
    var note: String? = null

    @Element
    var success = mutableListOf<RSuccess>()

    @Element
    var count = mutableListOf<RCount>()
}

@Xml(name = "success")
class RSuccess : Serializable {

    @Attribute(name = "repetition")
    var repetition: Int? = null

    @Attribute(name = "unique")
    var unique: Int? = null
}

@Xml(name = "count")
class RCount : Serializable {

    @Attribute(name = "iteration")
    var iteration: Int? = null

    @Attribute(name = "rejected")
    var rejected: Int? = null
}