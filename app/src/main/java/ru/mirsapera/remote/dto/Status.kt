package ru.mirsapera.remote.dto

import com.google.gson.annotations.SerializedName

class RStatus {

    @SerializedName("id")
    var id: String? = null

    @SerializedName("uid")
    var uid: String? = null

    @SerializedName("status")
    var status: String? = null

    @SerializedName("message")
    var message: String? = null

    val isSuccessful: Boolean
        get() = status == "success" || status == "sucess"
}