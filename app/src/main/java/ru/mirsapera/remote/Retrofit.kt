package ru.mirsapera.remote

import com.google.gson.ExclusionStrategy
import com.google.gson.FieldAttributes
import com.google.gson.annotations.SerializedName
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Invocation
import retrofit2.Retrofit
import java.io.IOException
import java.lang.reflect.Type

@MustBeDocumented
@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER
)
@Retention(AnnotationRetention.RUNTIME)
annotation class Tag(val value: String)

@MustBeDocumented
@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER
)
@Retention(AnnotationRetention.RUNTIME)
annotation class XmlResponse

@MustBeDocumented
@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER
)
@Retention(AnnotationRetention.RUNTIME)
annotation class JsonResponse

class SerializedNameStrategy : ExclusionStrategy {

    override fun shouldSkipField(attributes: FieldAttributes): Boolean {
        return attributes.getAnnotation(SerializedName::class.java) == null
    }

    override fun shouldSkipClass(clazz: Class<*>): Boolean {
        return false
    }
}

class TagInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val builder = request.newBuilder()
        request.tag(Invocation::class.java)?.let {
            it.method().getAnnotation(Tag::class.java)?.let { tag ->
                builder.tag(tag.value)
            }
        }
        return chain.proceed(builder.build())
    }
}

class MultipleConverterFactory(private val factories: Map<Class<*>, Converter.Factory>) :
    Converter.Factory() {

    override fun responseBodyConverter(
        type: Type,
        annotations: Array<Annotation>,
        retrofit: Retrofit
    ): Converter<ResponseBody, *>? {
        return annotations.mapNotNull { factories[it.annotationClass.javaObjectType] }
            .getOrNull(0)
            ?.responseBodyConverter(type, annotations, retrofit)
    }

    class Builder {

        private val factories = hashMapOf<Class<*>, Converter.Factory>()

        fun setXmlConverterFactory(converterFactory: Converter.Factory): Builder {
            factories[XmlResponse::class.java] = converterFactory
            return this
        }

        fun setJsonConverterFactory(converterFactory: Converter.Factory): Builder {
            factories[JsonResponse::class.java] = converterFactory
            return this
        }

        @Suppress("unused")
        fun addCustomConverterFactory(
            annotation: Class<out Annotation>,
            converterFactory: Converter.Factory
        ): Builder {
            factories[annotation] = converterFactory
            return this
        }

        fun build(): MultipleConverterFactory {
            return MultipleConverterFactory(factories)
        }
    }
}