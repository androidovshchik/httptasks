package ru.mirsapera.service

import android.content.Context
import androidx.work.Worker
import androidx.work.WorkerParameters
import org.kodein.di.KodeinAware
import org.kodein.di.android.closestKodein

abstract class BaseWorker(context: Context, parameters: WorkerParameters) :
    Worker(context, parameters), KodeinAware {

    override val kodein by closestKodein(context)
}