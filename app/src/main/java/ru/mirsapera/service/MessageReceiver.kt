package ru.mirsapera.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import androidx.annotation.StringRes
import androidx.core.app.NotificationCompat
import org.jetbrains.anko.activityManager
import org.jetbrains.anko.notificationManager
import ru.mirsapera.R
import ru.mirsapera.extension.getTopActivity
import ru.mirsapera.extension.pendingActivityFor
import ru.mirsapera.screen.base.listeners.IMessage
import ru.mirsapera.screen.main.MainActivity
import java.lang.ref.WeakReference

class MessageReceiver : BroadcastReceiver {

    private var reference: WeakReference<IMessage>? = null

    constructor() : super()

    constructor(listener: IMessage) : super() {
        reference = WeakReference(listener)
    }

    override fun onReceive(context: Context, intent: Intent) {
        context.run {
            val id = intent.getIntExtra("id", 0)
            val message = intent.getStringExtra("message") ?: return
            reference?.get()?.let {
                it.showMessage(message)
                return
            }
            if (activityManager.getTopActivity(packageName) == null) {
                notificationManager.notify(
                    id, NotificationCompat.Builder(applicationContext, "main")
                        .setSmallIcon(R.drawable.ic_notifications_white_24dp)
                        .setContentTitle("Внимание")
                        .setContentText(message)
                        .setContentIntent(pendingActivityFor<MainActivity>())
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setAutoCancel(true)
                        .build()
                )
            }
        }
    }

    companion object {

        fun sendMessage(context: Context, @StringRes resId: Int, id: Int? = null) = context.run {
            sendMessage(applicationContext, getString(resId), id)
        }

        fun sendMessage(context: Context, message: String, id: Int? = null) = context.run {
            sendBroadcast(Intent("$packageName.MESSAGE").apply {
                putExtra("id", id ?: (1000_000_000..Int.MAX_VALUE).random())
                putExtra("message", message)
            })
        }
    }
}