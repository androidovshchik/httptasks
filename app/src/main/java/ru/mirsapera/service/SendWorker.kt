package ru.mirsapera.service

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.work.*
import okhttp3.OkHttpClient
import org.jetbrains.anko.connectivityManager
import org.jetbrains.anko.notificationManager
import org.kodein.di.generic.instance
import ru.mirsapera.R
import ru.mirsapera.extension.cancelAll
import ru.mirsapera.extension.isConnected
import ru.mirsapera.remote.Server
import timber.log.Timber
import java.util.concurrent.TimeUnit

class SendWorker(context: Context, parameters: WorkerParameters) : BaseWorker(context, parameters) {

    private val client: OkHttpClient by instance()

    private val server: Server by instance()

    override fun doWork(): Result = try {
        val id = inputData.getInt("id", 0)
        val cancel = inputData.getBoolean("cancel", false)
        val note = inputData.getString("note").toString()
        val hash = inputData.getString("hash").toString()
        applicationContext.run {
            if (connectivityManager.isConnected) {
                val rStatus = server.sendTask(id, cancel, note, hash).execute().body()
                if (rStatus != null) {
                    rStatus.message?.let {
                        MessageReceiver.sendMessage(applicationContext, it, id)
                    }
                    if (rStatus.isSuccessful) {
                        Result.success()
                    } else {
                        Result.retry()
                    }
                } else {
                    Result.retry()
                }
            } else {
                MessageReceiver.sendMessage(applicationContext, R.string.network_error)
                Result.retry()
            }
        }
    } catch (e: Throwable) {
        Timber.e(e)
        Result.retry()
    }

    override fun onStopped() {
        val id = inputData.getInt("id", 0)
        client.cancelAll(NAME)
        applicationContext.notificationManager.cancel(id)
    }

    companion object {

        const val NAME = "SEND"

        fun launch(context: Context, params: Map<String, Any?>): LiveData<WorkInfo>? {
            val request = OneTimeWorkRequestBuilder<SendWorker>()
                .setInputData(
                    Data.Builder()
                        .putAll(params)
                        .build()
                )
                .setBackoffCriteria(BackoffPolicy.EXPONENTIAL, 10, TimeUnit.MINUTES)
                .build()
            return WorkManager.getInstance(context).run {
                enqueue(request)
                getWorkInfoByIdLiveData(request.id)
            }
        }
    }
}